package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	//private static Connection connection;

	public static synchronized DBManager getInstance() {
		if(instance==null){
			instance = new DBManager();
		}
		return instance;
	}
	private static final String URL = "jdbc:mysql://localhost:3306/test2db";
	private static final String USER = "root";
	private static final String PASSWORD = "root";

	public Connection getConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(URL,USER,PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	private DBManager() {
		getConnection();
		/*try {
			connection = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
		} catch (SQLException e) {
			e.printStackTrace();
		}*/
	}

	public List<User> findAllUsers() throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		List<User> userList = new ArrayList<>();
		try {
			preparedStatement = connection.prepareStatement("SELECT * FROM users");
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()){
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		try {
		    preparedStatement = connection.prepareStatement("INSERT INTO users(id,login) values(?,?)");
			preparedStatement.setInt(1, user.getId());
			preparedStatement.setString(2, user.getLogin());
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		for (User user : users) {
			try {
				preparedStatement = connection.prepareStatement("DELETE FROM users where id = ?");
				preparedStatement.setInt(1,user.getId());
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		User user = new User();
		try {
			preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE login = ?");
			preparedStatement.setString(1,login);
			ResultSet rs = preparedStatement.executeQuery();
			if(rs.next()){
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		Team team = new Team();
		try {
			preparedStatement = connection.prepareStatement("SELECT * FROM teams where name = ?");
			preparedStatement.setString(1,name);
			ResultSet rs = preparedStatement.executeQuery();
			if(rs.next()){
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		List<Team> teamList = new ArrayList<>();
		try {
			preparedStatement = connection.prepareStatement("SELECT * FROM teams");
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()){
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teamList.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("INSERT INTO teams(id,name) values(?,?)");
			preparedStatement.setInt(1, team.getId());
			preparedStatement.setString(2, team.getName());
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
//3
	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;

		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			for (Team team:teams){
				preparedStatement = connection.prepareStatement("insert into users_teams(user_id,team_id) value (?,?)");
				preparedStatement.setInt(1,user.getId());
				preparedStatement.setInt(2,team.getId());
				preparedStatement.execute();
			}
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return false;	
	}

	public List<Team> getUserTeams(User user) throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		List<Team> teamList = new ArrayList<>();
		try {
			preparedStatement = connection.prepareStatement("select * FROM teams where id in(select team_id from users_teams where user_id = ?)");
			preparedStatement.setInt(1,user.getId());
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teamList.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teamList;
	}
//4
	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
			try {
				preparedStatement = connection.prepareStatement("DELETE FROM teams where id = ?");
				preparedStatement.setInt(1,team.getId());
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("UPDATE teams set name = ? where id = ?");
			preparedStatement.setString(1,team.getName());
			preparedStatement.setInt(2,team.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
